const Task = require('../models/Task.js');

module.exports.getAllTasks = () => {

	return Task.find({}).then(result => {
		return result
	})
};

module.exports.getTask = (taskID) => {
	return Task.findById(taskID).then((result, error) => {
		if(error) {
			return console.log(error)
		} else {
			return result
		}
	})
};

module.exports.createTasks = (reqBody) => {
	let newTask = new Task({
		name: reqBody.name
	})
	return newTask.save().then((savedTask, error) => {
	
		if(error) {
			return console.log(error)
		} else if (savedTask == null || savedTask.name == reqBody.name) {
			return 'Duplicate Task Found!'
		} else {
			return savedTask //'Task created successfully!'
		}
		
	})
}	

module.exports.updateTasks = (taskID, newContent) => {
	return Task.findById(taskID).then((result, error) => {
		if(error) {
			return console.log(error)
		} else {
			result.name = newContent.name

			return result.save().then((updatedTasks, error) => {
				if (error){
					return console.log(error)
				} else {
					return updatedTasks
				}
			})
		}
	})
};

// update status

module.exports.updateStatus = (taskID, newStatus) => {
	return Task.findById(taskID).then((result, error) => {
		if(error) {
			return console.log(error)
		} else {
			result.status = newStatus.status

			return result.save().then((updatedStatus, error) => {
				if (error){
					return console.log(error)
				} else {
					return updatedStatus
				}
			})
		}
	})
};

module.exports.deleteTasks = (taskID) => {
	return Task.findByIdAndRemove(taskID).then((deletedTasks, error) => {
		if(error) {
			return console.log(error)
		} else {
			return deletedTasks
		}
	})
}