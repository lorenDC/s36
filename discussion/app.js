const express = require('express');
const mongoose = require('mongoose');
const taskRoutes = require('../routes/taskRoutes.js');
const app = express();
const port = 3001;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect(`mongodb://lrndlacrz:admin123@ac-mvlgpdf-shard-00-00.c2nhgts.mongodb.net:27017,ac-mvlgpdf-shard-00-01.c2nhgts.mongodb.net:27017,ac-mvlgpdf-shard-00-02.c2nhgts.mongodb.net:27017/S36?ssl=true&replicaSet=atlas-ika0ge-shard-0&authSource=admin&retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection'));
db.once('open', () => console.log('Connectd to server!'));


// Middlewares
app.use('/tasks', taskRoutes);


app.listen(port, () => console.log(`Server is runnung at ${port}`))