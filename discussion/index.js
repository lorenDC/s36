/* 1. Create a readingListActE folder. Inside create an index.js file.
2. Install nodemon and perform npm init command in gitbash to create a package.json file.
3. Install express and mongoose. Make sure to check these dependencies insde the package.json file.
4. Create a .gitignore file and store the node_modules in it.
5. Once done with your solution, create a repo named "readingListActE" and push your solution.
6. Save the repo link on S35-C1: Express.js: Data Persistende via Mongoos ODM */

/*

Activity Instructions:
1. Create a User schema with the following fields: firstName, lastName, username, password, email.
2. Create a Product schema with the following fields: name, description, price.
3. Create a User Model and  a Product Model.
4. Create a POST request that will access the /register route which will create a user. Test this in Postman app.
5. Create a POST request that will access the /createProduct route which will create a product. Test this in Postman app.
6. Create a GET request that will access the /users route to retrieve all users from your DB. Test this in Postman.
7. Create a GET request that will access the /products route to retrieve all products from your DB. Test this in Postman.

Note: create a separate Database collection named S35-C1 in your MongoDb account.
*/


// Code here:

const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port = 5001;

mongoose.connect(`mongodb://lrndlacrz:admin123@ac-mvlgpdf-shard-00-00.c2nhgts.mongodb.net:27017,ac-mvlgpdf-shard-00-01.c2nhgts.mongodb.net:27017,ac-mvlgpdf-shard-00-02.c2nhgts.mongodb.net:27017/S35?ssl=true&replicaSet=atlas-ika0ge-shard-0&authSource=admin&retryWrites=true&w=majority`,{
		useNewUrlParser: true,
		useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection'));
db.once('open', () => console.log('Connected to mongoose!'));

const userSchema = new mongoose.Schema({
	firstName: String,
	lastName: String,
	username: String,
	password: String,
	email: String
});

const productSchema = new mongoose.Schema({
	name: String,
	description: String,
	price: Number
});

const User = mongoose.model('User', userSchema);
const Product = mongoose.model('Product', productSchema);

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// post register
app.post('/register', (request, response) => {
	User.find({username: request.body.username}, (error, result) => {
		if(error){
			return response.send(error);
		} else if (result == null || result.username == request.body.username) {
			return response.send('Username already exists!');
		} else {
			let newUser = new User({
				firstName: request.body.firstName,
				lastName: request.body.lastName,
				username: request.body.username,
				password: request.body.password,
				email: request.body.email
			});
			newUser.save((error, addUser) => {
				if(error){
					return response.send(error);
				} else {
					return response.status(201).send('New user added!')
				}
			})
		}
	})
})


// get users
app.get('/users', (request, response) => {
	User.find({}, (error, results) => {
		if(error){
			return respose.send(error);
		} else {
			response.status(200).json({
				register: results
			})
		}
	})
})

// post product
app.post('/createProduct', (request, response) => {
	Product.find({name: request.body.name}, (error, result) => {
		if(error){
			return response.send(error);
		} else if (result == null || result.username == request.body.name) {
			return response.send('Product already exists!');
		} else {
			let newProduct = new Product({
				name: request.body.name,
				description: request._body.description,
				price: request.body.price
			});
			newProduct.save((error, addUser) => {
				if(error){
					return response.send(error);
				} else {
					return response.status(201).send('New product added!')
				}
			})
		}
	})
})

// get products
app.get('/products', (request, response) => {
	Product.find({}, (error, results) => {
		if(error){
			return respose.send(error);
		} else {
			response.status(200).json({
				register: results
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at port: ${port}`));


