const mongoose = require('mongoose');

const taskSChema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: 'Pending'
	}
});

module.exports = mongoose.model('Task', taskSChema)