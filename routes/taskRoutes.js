const express = require('express');
const TaskController = require('../controllers/TaskController.js')
const router = express.Router();

// Routes
router.get('/', (request, response) => {
	TaskController.getAllTasks().then(resultFromController => response.send(resultFromController))
})

router.get('/:id', (request, response) => {
	TaskController.getTask().then(resultFromController => response.send(resultFromController))
})

router.post('/create', (request, response) => {
	TaskController.createTasks(request.body).then(resultFromController => response.send(resultFromController))
})

router.put('/:id/update', (request, response) => {
	TaskController.updateTasks(request.params.id, request.body).then(resultFromController => response.send(resultFromController))
})

router.put('/:id', (request, response) => {
	TaskController.updateStatus(request.params.id, request.body).then(resultFromController => response.send(resultFromController))
})

router.delete('/:id/delete', (request, response) => {
	TaskController.deleteTasks(request.params.id, request.body).then(resultFromController => response.send(resultFromController))
})

module.exports = router